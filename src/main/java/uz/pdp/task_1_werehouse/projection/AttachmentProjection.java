package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.Attachment;

@Projection(types = Attachment.class)
public interface AttachmentProjection {
    Integer getId();
    String getName();
    double getSize();
    String getContentType();
}
