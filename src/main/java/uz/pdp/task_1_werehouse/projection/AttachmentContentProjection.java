package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.Attachment;
import uz.pdp.task_1_werehouse.entity.AttachmentContent;

@Projection(types = AttachmentContent.class)
public interface AttachmentContentProjection {
    Integer getId();
    byte[] getContent();
    String getAttachment();
}
