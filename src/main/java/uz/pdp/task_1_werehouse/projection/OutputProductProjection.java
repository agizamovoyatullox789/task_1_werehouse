package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.*;

import java.util.Date;

@Projection(types = OutputProduct.class)
public interface OutputProductProjection {
    Integer getId();
    Product getProduct();
    double getAmount();
    double getPrice();
    Output getOutput();
}
