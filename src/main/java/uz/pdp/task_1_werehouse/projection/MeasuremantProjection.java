package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.*;

@Projection(types = Measuremant.class)
public interface MeasuremantProjection {
    Integer getId();
    String getName();
    boolean getActive();

}
