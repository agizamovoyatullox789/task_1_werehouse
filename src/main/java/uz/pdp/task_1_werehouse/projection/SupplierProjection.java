package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.*;

@Projection(types = Supplier.class)
public interface SupplierProjection {
    Integer getId();
    String getName();
    String getPhoneNumber();
    boolean getActive();
}
