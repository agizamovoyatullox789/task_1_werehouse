package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.Category;
import uz.pdp.task_1_werehouse.entity.Client;

@Projection(types = Client.class)
public interface ClientProjection {
    Integer getId();
    String getName();
    String getPhoneNumber();
}
