package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.*;

import java.util.Date;

@Projection(types = InputProduct.class)
public interface InputProductProjection {
    Integer getId();
    Product getProduct();
    double getAmount();
    double getPrice();
    Date getExpireDate();
    Input getInput();
}
