package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.*;

import java.util.Date;

@Projection(types = Product.class)
public interface ProductProjection {
    Integer getId();
    String getName();
    Category getCategory();
    Attachment getPhoto();
    String getCode();
    Measuremant getMeasuremant();
    boolean getActive();
}
