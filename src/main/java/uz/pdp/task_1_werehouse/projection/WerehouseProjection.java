package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.Category;
import uz.pdp.task_1_werehouse.entity.WereHouse;

@Projection(types = WereHouse.class)
public interface WerehouseProjection {
    Integer getId();
    String getName();
    boolean getActive();
}
