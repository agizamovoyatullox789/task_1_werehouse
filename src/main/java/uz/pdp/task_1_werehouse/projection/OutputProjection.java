package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.*;

import java.util.Date;

@Projection(types = Output.class)
public interface OutputProjection {
    Integer getId();
    Date getDate();
    WereHouse getWereHouse();
    Currency getCurrency();
    String getFactureNumber();
    String getCode();
    Client getClient();
}
