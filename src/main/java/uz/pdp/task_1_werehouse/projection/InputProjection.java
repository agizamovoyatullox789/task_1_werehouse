package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.Currency;
import uz.pdp.task_1_werehouse.entity.Input;
import uz.pdp.task_1_werehouse.entity.Supplier;
import uz.pdp.task_1_werehouse.entity.WereHouse;

@Projection(types = Input.class)
public interface InputProjection {
    Integer getId();
    WereHouse getWereHouse();
    Supplier getSupplier();
    Currency getCurrency();
    String getFactureNumber();
    String getCode();
}
