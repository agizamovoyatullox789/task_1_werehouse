package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.Category;

@Projection(types = Category.class)
public interface CategoryProjection {
    Integer getId();
    String getName();
    Integer getParentCategoryId();
    boolean getActive();
}
