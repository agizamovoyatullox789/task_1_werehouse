package uz.pdp.task_1_werehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.task_1_werehouse.entity.Supplier;
import uz.pdp.task_1_werehouse.entity.User;

@Projection(types = User.class)
public interface UserProjection {
    Integer getId();

    String getFirstName();

    String getLastName();

    String getPhoneNumber();

    String getCode();

    String getPassword();

    boolean getActive();
}
