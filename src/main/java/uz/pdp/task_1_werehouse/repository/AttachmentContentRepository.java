package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Attachment;
import uz.pdp.task_1_werehouse.entity.AttachmentContent;
import uz.pdp.task_1_werehouse.projection.AttachmentContentProjection;
import uz.pdp.task_1_werehouse.projection.AttachmentProjection;

@RepositoryRestResource(path = "attechmentContent",collectionResourceRel = "list",excerptProjection = AttachmentContentProjection.class)
public interface AttachmentContentRepository extends JpaRepository<AttachmentContent,Integer> {
}
