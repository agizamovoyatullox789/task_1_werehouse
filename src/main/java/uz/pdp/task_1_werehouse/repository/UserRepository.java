package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Supplier;
import uz.pdp.task_1_werehouse.entity.User;
import uz.pdp.task_1_werehouse.projection.SupplierProjection;
import uz.pdp.task_1_werehouse.projection.UserProjection;

@RepositoryRestResource(path = "user",collectionResourceRel = "list",excerptProjection = UserProjection.class)
public interface UserRepository extends JpaRepository<User,Integer> {
}
