package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Input;
import uz.pdp.task_1_werehouse.entity.InputProduct;
import uz.pdp.task_1_werehouse.projection.InputProductProjection;
import uz.pdp.task_1_werehouse.projection.InputProjection;

@RepositoryRestResource(path = "inputProduct",collectionResourceRel = "list",excerptProjection = InputProductProjection.class)
public interface InputProductRepository extends JpaRepository<InputProduct,Integer> {
}
