package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Category;
import uz.pdp.task_1_werehouse.entity.Client;
import uz.pdp.task_1_werehouse.projection.CategoryProjection;
import uz.pdp.task_1_werehouse.projection.ClientProjection;

@RepositoryRestResource(path = "client",collectionResourceRel = "list",excerptProjection = ClientProjection.class)
public interface ClientRepository extends JpaRepository<Client,Integer> {
}
