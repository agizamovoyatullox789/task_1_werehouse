package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.InputProduct;
import uz.pdp.task_1_werehouse.entity.OutputProduct;
import uz.pdp.task_1_werehouse.projection.InputProductProjection;
import uz.pdp.task_1_werehouse.projection.OutputProductProjection;

@RepositoryRestResource(path = "outputProduct",collectionResourceRel = "list",excerptProjection = OutputProductProjection.class)
public interface OutputProductRepository extends JpaRepository<OutputProduct,Integer> {
}
