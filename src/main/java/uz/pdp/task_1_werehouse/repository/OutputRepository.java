package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Input;
import uz.pdp.task_1_werehouse.entity.Output;
import uz.pdp.task_1_werehouse.projection.InputProjection;
import uz.pdp.task_1_werehouse.projection.OutputProjection;

@RepositoryRestResource(path = "output",collectionResourceRel = "list",excerptProjection = OutputProjection.class)
public interface OutputRepository extends JpaRepository<Output,Integer> {
}
