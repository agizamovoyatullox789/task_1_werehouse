package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Client;
import uz.pdp.task_1_werehouse.entity.Input;
import uz.pdp.task_1_werehouse.projection.CurrencyProjection;
import uz.pdp.task_1_werehouse.projection.InputProjection;

@RepositoryRestResource(path = "input",collectionResourceRel = "list",excerptProjection = InputProjection.class)
public interface InputRepository extends JpaRepository<Input,Integer> {
}
