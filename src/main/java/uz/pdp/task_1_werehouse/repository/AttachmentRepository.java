package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Attachment;
import uz.pdp.task_1_werehouse.projection.AttachmentProjection;

@RepositoryRestResource(path = "attechment",collectionResourceRel = "list",excerptProjection = AttachmentProjection.class)
public interface AttachmentRepository extends JpaRepository<Attachment,Integer> {
}
