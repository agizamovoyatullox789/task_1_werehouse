package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Attachment;
import uz.pdp.task_1_werehouse.entity.Category;
import uz.pdp.task_1_werehouse.projection.AttachmentProjection;
import uz.pdp.task_1_werehouse.projection.CategoryProjection;

@RepositoryRestResource(path = "category",collectionResourceRel = "list",excerptProjection = CategoryProjection.class)
public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
