package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Output;
import uz.pdp.task_1_werehouse.entity.Product;
import uz.pdp.task_1_werehouse.projection.OutputProjection;
import uz.pdp.task_1_werehouse.projection.ProductProjection;

@RepositoryRestResource(path = "product",collectionResourceRel = "list",excerptProjection = ProductProjection.class)
public interface ProductRepository extends JpaRepository<Product,Integer> {
}
