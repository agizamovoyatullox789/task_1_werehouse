package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Client;
import uz.pdp.task_1_werehouse.entity.Currency;
import uz.pdp.task_1_werehouse.projection.ClientProjection;
import uz.pdp.task_1_werehouse.projection.CurrencyProjection;

@RepositoryRestResource(path = "currency",collectionResourceRel = "list",excerptProjection = CurrencyProjection.class)
public interface CurrencyRepository extends JpaRepository<Currency,Integer> {
}
