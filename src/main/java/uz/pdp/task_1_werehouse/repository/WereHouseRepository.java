package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.WereHouse;
import uz.pdp.task_1_werehouse.projection.WerehouseProjection;

@RepositoryRestResource(path = "werehouse",collectionResourceRel = "list",excerptProjection = WerehouseProjection.class)
public interface WereHouseRepository extends JpaRepository<WereHouse,Integer> {
}
