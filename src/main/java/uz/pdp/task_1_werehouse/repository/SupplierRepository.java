package uz.pdp.task_1_werehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task_1_werehouse.entity.Product;
import uz.pdp.task_1_werehouse.entity.Supplier;
import uz.pdp.task_1_werehouse.projection.ProductProjection;
import uz.pdp.task_1_werehouse.projection.SupplierProjection;

@RepositoryRestResource(path = "supplier",collectionResourceRel = "list",excerptProjection = SupplierProjection.class)
public interface SupplierRepository extends JpaRepository<Supplier,Integer> {
}
