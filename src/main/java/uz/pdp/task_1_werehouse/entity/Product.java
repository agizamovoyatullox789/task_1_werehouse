package uz.pdp.task_1_werehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task_1_werehouse.entity.template.AbsName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends AbsName {

    @ManyToOne(optional = false)
    private Category category;

    @OneToOne
    private Attachment photo;

    @Column(nullable = false)
    private String code;

    @ManyToOne
    private Measuremant measuremant;

    @Column(nullable = false)
    private boolean active;
}
