package uz.pdp.task_1_werehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task_1_werehouse.entity.template.AbsDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Input extends AbsDate {
    @ManyToOne
    private WereHouse wereHouse;

    @ManyToOne
    private Supplier supplier;

    @ManyToOne
    private Currency currency;

    private  String factureNumber;

    @Column(nullable = false)
    private String code;
}
