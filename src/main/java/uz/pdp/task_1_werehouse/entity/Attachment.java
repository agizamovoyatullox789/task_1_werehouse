package uz.pdp.task_1_werehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task_1_werehouse.entity.template.AbsName;

import javax.persistence.Entity;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Attachment extends AbsName {

    private double size;

    private String contentType;
}
