package uz.pdp.task_1_werehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task_1_werehouse.entity.template.AbsName;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Client extends AbsName {
    @Column(nullable = false,unique = true)
    private String phoneNumber;
}
