package uz.pdp.task_1_werehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task1WereHouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(Task1WereHouseApplication.class, args);
    }

}
